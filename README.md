Sistema de loja virtual com cadastro de clientes, produtos e pedidos.

Para gerar o token de acesso aos endpoints restritos, via Postman:

1. Fazer o POST para o endpoint "/login" informando email e senha (que estão disponíveis em DBService);
2. No HEADER, pegar o token que está na linha "authorization";
3. Inserir o token no HEADER na aba em que fará o GET.


Para o envio de e-mail:

1. No arquivo .properties (dev ou test) informar o usuário e senha de uma conta Google
2. application.properties, informar os e-mails default para envio e recebimento



