package com.brmendes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrmendesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrmendesApplication.class, args);
	}

}
